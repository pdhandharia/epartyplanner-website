﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

/// <summary>
/// Contains methods which provide business logic for the application
/// </summary>
public class BusinessAccessLayer
{
    DataAccessLayer dal;
    public BusinessAccessLayer()
	{
		//
		// TODO: Add constructor logic here
		//
        dal = new DataAccessLayer();
	}

    
    /// <summary>
    /// Checks whether the username entered by the user is available.
    /// Also ensures that the username is not from among the list of restricted usernames.
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    
    public bool CheckUsername(string username)
    {
        return true;
    }


    /// <summary>
    /// Inserts a new customer record into the database.
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="lname"></param>
    /// <param name="minit"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="pincode"></param>
    /// <param name="city"></param>
    /// <param name="mobile"></param>
    /// <param name="email"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <param name="imgurl"></param>
    /// <returns></returns>
    
    public int NewCustomer(string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, string username, string password, string question, string answer)
    {
        return 1;
    }


    /// <summary>
    /// Inserts a new restaurant record into the database.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="city"></param>
    /// <param name="pincode"></param>
    /// <param name="email"></param>
    /// <param name="contact"></param>
    /// <param name="landmark"></param>
    /// <param name="area"></param>
    /// <param name="owner"></param>
    /// <param name="timings"></param>
    /// <param name="website"></param>
    /// <param name="hasAC"></param>
    /// <param name="takesCC"></param>
    /// <param name="alcohol"></param>
    /// <param name="grade"></param>
    /// <param name="primCuisine"></param>
    /// <param name="secCuisine"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <returns></returns>

    public int NewRest(string name, string addr1, string addr2, string city, string pincode, string email, string contact, string landmark, string area, string owner, string timings, string website, bool hasAC, bool takesCC, bool alcohol, char grade, string primCuisine, string secCuisine, string username, string password, string question, string answer)
    {
        return 1;
    }

    /// <summary>
    /// Inserts a new SP record in the database.
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="lname"></param>
    /// <param name="minit"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="pincode"></param>
    /// <param name="city"></param>
    /// <param name="mobile"></param>
    /// <param name="email"></param>
    /// <param name="exp"></param>
    /// <param name="cat"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <returns></returns>

    public int NewSP(string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, int exp, string cat, string username, string password, string question, string answer)
    {
        return 1;
    }


    /// <summary>
    /// Checks if the specified exists.
    /// If the user exists, retrieve user details
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>

    public bool CheckLogin(string username, string password)
    {
        DataSet ds = dal.LoginDetails(username, password);
        if (ds.Tables["LoginDetails"].Rows.Count > 0)
            return true;
        else
            return false;
    }

    public DataTable GetRestaurantDetails(string rest_id)
    {
        try
        {
            DataSet ds = dal.GetRestaurants();
            DataRow[] rows;
            rows = ds.Tables[0].Select("rest_id = " + rest_id);
            DataTable t = new DataTable("restaurants");
            t.Rows.Add(rows[0]);
            return t;
        }
        catch (Exception err)
        {
            Console.Write(err.Message);
            return null;
        }
    }
}
