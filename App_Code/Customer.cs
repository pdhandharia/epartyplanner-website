﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Customer
/// </summary>
[Serializable]
public class Customer
{
    private string _greeting;

    public string Greeting
    {
        get { return _greeting; }
        set { _greeting = value; }
    }
    private string _usertype;

    public string Usertype
    {
        get { return _usertype; }
        set { _usertype = value; }
    }
    public Customer(string greeting, string usertype)
	{
		//
		// TODO: Add constructor logic here
		//
        Usertype = usertype;
        Greeting = greeting;               
	}
}
