﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;


/// <summary>
/// Contains methods to access database to insert, update, retrieve and delete records.
/// </summary>
public class DataAccessLayer
{
    string connStr;
    public DataAccessLayer()
    {
        connStr = ConfigurationManager.ConnectionStrings["eppConnStr"].ToString();
    }

    /// <summary>
    /// Inserts new customer record in the database.
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="minit"></param>
    /// <param name="lname"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="city"></param>
    /// <param name="pincode"></param>
    /// <param name="email"></param>
    /// <param name="mobile"></param>
    /// <param name="answer"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="username"></param>
    /// <param name="cust_id"></param>
    /// <param name="imgurl"></param>
    /// <returns></returns>


    public int InsertCustomer(string cust_id, string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, string username, string password, string question, string answer, string imgurl)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("newCustomer", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        try
        {
            cmd.Parameters.AddWithValue("@cust_id", cust_id);
            cmd.Parameters.AddWithValue("@fname", fname);
            cmd.Parameters.AddWithValue("@minit", minit);
            cmd.Parameters.AddWithValue("@lname", lname);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);
            cmd.Parameters.AddWithValue("@imgurl", imgurl);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }



    /// <summary>
    /// Inserts new service provider record in the database.
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="minit"></param>
    /// <param name="lname"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="city"></param>
    /// <param name="pincode"></param>
    /// <param name="email"></param>
    /// <param name="mobile"></param>
    /// <param name="exp"></param>
    /// <param name="catID"></param>
    /// <param name="answer"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="username"></param>
    /// <param name="sp_id"></param>
    /// <param name="rating"></param>
    /// <param name="imgurl"></param>
    /// <returns></returns>

    public int InsertSP(string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, int exp, string cat, string username, string password, string question, string answer, string sp_id, float rating, string imgurl)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("newSP", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        try
        {
            cmd.Parameters.AddWithValue("@sp_id", sp_id);
            cmd.Parameters.AddWithValue("@fname", fname);
            cmd.Parameters.AddWithValue("@minit", minit);
            cmd.Parameters.AddWithValue("@lname", lname);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@experience", exp);
            cmd.Parameters.AddWithValue("@catID", cat);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);
            cmd.Parameters.AddWithValue("@rating", rating);
            cmd.Parameters.AddWithValue("@imgurl", imgurl);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }


    /// <summary>
    /// Inserts new restaurant record in the database.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="city"></param>
    /// <param name="pincode"></param>
    /// <param name="email"></param>
    /// <param name="contact_no"></param>
    /// <param name="landmark"></param>
    /// <param name="area"></param>
    /// <param name="owner"></param>
    /// <param name="timings"></param>
    /// <param name="website"></param>
    /// <param name="hasAC"></param>
    /// <param name="takesCC"></param>
    /// <param name="alcohol"></param>
    /// <param name="grade"></param>
    /// <param name="primCuisine"></param>
    /// <param name="secCuisine"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <param name="rest_id"></param>
    /// <param name="imgurl"></param>
    /// <returns></returns>

    public int InsertRest(string name, string addr1, string addr2, string pincode, string city, string landmark, string area, string contact_no, string owner, string email, string website, string timings, bool hasAC, bool takesCC, bool alcohol, char grade, string primCuisine, string secCuisine, string username, string password, string question, string answer, string rest_id, string imgurl)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("newRest", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        try
        {
            cmd.Parameters.AddWithValue("@rest_id", rest_id);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@landmark", landmark);
            cmd.Parameters.AddWithValue("@area", area);
            cmd.Parameters.AddWithValue("@contact_no", contact_no);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@website", website);
            cmd.Parameters.AddWithValue("@owner", owner);
            cmd.Parameters.AddWithValue("@timings", timings);
            cmd.Parameters.AddWithValue("@hasAC", hasAC);
            cmd.Parameters.AddWithValue("@takesCC", takesCC);
            cmd.Parameters.AddWithValue("@alcohol", alcohol);
            cmd.Parameters.AddWithValue("@grade", grade);
            cmd.Parameters.AddWithValue("@primCuisine", primCuisine);
            cmd.Parameters.AddWithValue("@secCuisine", secCuisine);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);
            cmd.Parameters.AddWithValue("@imgurl", imgurl);
            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }



    /// <summary>
    /// Return dataset containing login details of requested user.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>

    public DataSet LoginDetails(string username, string password)
    {
        SqlConnection conn = new SqlConnection(connStr);
        SqlDataAdapter adp = new SqlDataAdapter("loginCheck", conn);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@password", password);

        DataSet ds = new DataSet();

        try
        {
            adp.Fill(ds, "LoginDetails");
            return ds;
        }
        catch
        {
            throw;
        }
        finally
        {
            ds.Dispose();
            adp.Dispose();
            conn.Close();
            conn.Dispose();
        }
    }
    /// <summary>
    /// To change password
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public int ChangePassword(string username, string password)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("changePSW", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }



    /// <summary>
    /// to add a bookmark
    /// </summary>
    /// <param name="bookmark_in"></param>
    /// <param name="rest_sp_id"></param>
    /// <returns></returns>
    public int AddBookmark(int bookmark_in, int rest_sp_id)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("addBookmark", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@bookmark_id", bookmark_in);
            cmd.Parameters.AddWithValue("@rest_sp_id", rest_sp_id);
            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }
    /// <summary>
    /// To Update a Customer profile
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="lname"></param>
    /// <param name="minit"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="pincode"></param>
    /// <param name="city"></param>
    /// <param name="mobile"></param>
    /// <param name="email"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <returns></returns>
    public int UpdateCustomer(string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, string username, string password, string question, string answer)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("updateCustomer", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@fname", fname);
            cmd.Parameters.AddWithValue("@minit", minit);
            cmd.Parameters.AddWithValue("@lname", lname);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }
    /// <summary>
    /// To update a SP profile
    /// </summary>
    /// <param name="fname"></param>
    /// <param name="lname"></param>
    /// <param name="minit"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="pincode"></param>
    /// <param name="city"></param>
    /// <param name="mobile"></param>
    /// <param name="email"></param>
    /// <param name="exp"></param>
    /// <param name="catID"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <returns></returns>
    public int UpdateSP(string fname, string lname, char minit, string addr1, string addr2, string pincode, string city, string mobile, string email, int exp, int catID, string username, string password, string question, string answer)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("updateSP", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        try
        {
            cmd.Parameters.AddWithValue("@fname", fname);
            cmd.Parameters.AddWithValue("@minit", minit);
            cmd.Parameters.AddWithValue("@lname", lname);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@experience", exp);
            cmd.Parameters.AddWithValue("@catID", catID);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }
    /// <summary>
    /// To update a Restaurant profile
    /// </summary>
    /// <param name="name"></param>
    /// <param name="addr1"></param>
    /// <param name="addr2"></param>
    /// <param name="pincode"></param>
    /// <param name="city"></param>
    /// <param name="landmark"></param>
    /// <param name="area"></param>
    /// <param name="contact_no"></param>
    /// <param name="owner"></param>
    /// <param name="email"></param>
    /// <param name="website"></param>
    /// <param name="timings"></param>
    /// <param name="hasAC"></param>
    /// <param name="takesCC"></param>
    /// <param name="alcohol"></param>
    /// <param name="grade"></param>
    /// <param name="primCuisine"></param>
    /// <param name="secCuisine"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="answer"></param>
    /// <returns></returns>
    public int UpdateRest(string name, string addr1, string addr2, string pincode, string city, string landmark, string area, string contact_no, string owner, string email, string website, string timings, bool hasAC, bool takesCC, bool alcohol, char grade, int primCuisine, int secCuisine, string username, string password, string question, string answer)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("updateRest", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        try
        {
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@addr1", addr1);
            cmd.Parameters.AddWithValue("@addr2", addr2);
            cmd.Parameters.AddWithValue("@pincode", pincode);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@landmark", landmark);
            cmd.Parameters.AddWithValue("@area", area);
            cmd.Parameters.AddWithValue("@contact_no", contact_no);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@website", website);
            cmd.Parameters.AddWithValue("@owner", owner);
            cmd.Parameters.AddWithValue("@timings", timings);
            cmd.Parameters.AddWithValue("@hasAC", hasAC);
            cmd.Parameters.AddWithValue("@takesCC", takesCC);
            cmd.Parameters.AddWithValue("@alcohol", alcohol);
            cmd.Parameters.AddWithValue("@grade", grade);
            cmd.Parameters.AddWithValue("@primCuisine", primCuisine);
            cmd.Parameters.AddWithValue("@secCuisine", secCuisine);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);
            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }
    /// <summary>
    /// To change secret question and answer
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="question"></param>
    /// <param name="anwser"></param>
    /// <returns></returns>
    public int ChangeQA(string username, string password, string question, string answer)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("changeQA", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@question", question);
            cmd.Parameters.AddWithValue("@answer", answer);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }

    }
    /// <summary>
    /// To insert a new review
    /// </summary>
    /// <param name="comments"></param>
    /// <param name="added_by"></param>
    /// <param name="added_for"></param>
    /// <returns></returns>
    public int NewReview(string comments, string added_by, string added_for)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("newReview", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@comments", comments);
            cmd.Parameters.AddWithValue("@added_by", added_by);
            cmd.Parameters.AddWithValue("@added_for", added_for);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }
    /// <summary>
    /// To insert a new order
    /// </summary>
    /// <param name="order_id"></param>
    /// <param name="delivery_on"></param>
    /// <param name="status"></param>
    /// <param name="amount"></param>
    /// <param name="placed_by"></param>
    /// <returns></returns>
    public int NewOrder(string order_id, DateTime delivery_on, string status, float amount, string placed_by)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("newOrders", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@order_id", order_id);
            cmd.Parameters.AddWithValue("@delivery_on",delivery_on);
            cmd.Parameters.AddWithValue("@status",status);
            cmd.Parameters.AddWithValue("@amount",amount);
            cmd.Parameters.AddWithValue("@placed_by",placed_by);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        } 
    }
    /// <summary>
    /// To cancel order
    /// </summary>
    /// <param name="order_id"></param>
    /// <returns></returns>
    public int CancelOrder(string order_id)
    {
        SqlConnection conn = new SqlConnection(connStr);
        conn.Open();
        SqlCommand cmd = new SqlCommand("cancelOrder", conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@order_id", order_id);

            return cmd.ExecuteNonQuery();
        }
        catch
        {
            throw;
        }
        finally
        {
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
        }
    }

    /// <summary>
    /// Returns the cuisines table used for restaurant registration and searching restaurants
    /// </summary>
    /// <returns></returns>
    public DataSet GetCuisines()
    {
        SqlConnection conn = new SqlConnection(connStr);
        SqlCommand cmd = new SqlCommand("getCuisines", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds, "cuisines");
        return ds;
    }

    public DataSet GetRestaurants()
    {
        SqlConnection conn = new SqlConnection(connStr);
        SqlCommand cmd = new SqlCommand("getRestaurants", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds, "Restaurants");
        return ds;
    }
}
