﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Restaurant
/// </summary>
public class Restaurant
{
    private string rest_id;
    private string name, address, pincode, city, area, landmark;
    private string contact, email, website, owner, timings;
    private bool hasAC, takesCC, alcohol;
    private int rating;
    private char grade;
    private string primCuisine, secCuisine;


    public Restaurant()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public string Landmark
    {
        get { return landmark; }
        set { landmark = value; }
    }

    public string Area
    {
        get { return area; }
        set { area = value; }
    }

    public string City
    {
        get { return city; }
        set { city = value; }
    }

    public string Pincode
    {
        get { return pincode; }
        set { pincode = value; }
    }

    public string Address
    {
        get { return address; }
        set { address = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    
    public string SecondaryCuisine
    {
        get { return secCuisine; }
        set { secCuisine = value; }
    }

    public string PrimaryCuisine
    {
        get { return primCuisine; }
        set { primCuisine = value; }
    }   
}
