﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" Title="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
<style>
    .pswdlink
    {
        font-size: 11px;
        font-weight: bold;
        margin-top: 5px;
    }
</style>
</asp:Content>


<asp:Content ID="navbar" ContentPlaceHolderID="cph_nav" runat="server">
    <div  id="nav" class="home">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
    </div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Login
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph_mainContent" Runat="Server">
<p>
    <asp:Label ID="lbl_error" runat="server" Visible="False" Font-Bold="True" 
        Font-Size="X-Small" ForeColor="#FF3300"></asp:Label>
    <br />
    <asp:Label ID="lbl_username" runat="server" Height="20px" Text="Username:" 
        Width="100px"></asp:Label>
&nbsp;<asp:TextBox ID="tb_username" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_username" runat="server" 
        ControlToValidate="tb_username">Please enter your username.</asp:RequiredFieldValidator>
</p>
<p>
    <asp:Label ID="lbl_password" runat="server" Height="20px" Text="Password:" 
        Width="100px"></asp:Label>
&nbsp;<asp:TextBox ID="tb_password" runat="server" EnableViewState="False" 
        TextMode="Password"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_password" runat="server" 
        ControlToValidate="tb_password">Please enter your password.</asp:RequiredFieldValidator>
</p>
<p>
    
    <asp:CheckBox ID="chk_rem" runat="server" Checked="True" Text="Remember me" />
    
</p>
    <p>
    
        <asp:Button ID="cmd_submit" runat="server" Text="Sign In" 
            onclick="cmd_submit_Click" />
        <br />
        <span class="pswdlink"><a href="forgot.aspx">Forgot Password?</a></span></p>
</asp:Content>

