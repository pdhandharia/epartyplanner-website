﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class login : System.Web.UI.Page
{
    BusinessAccessLayer bal = new BusinessAccessLayer();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cmd_submit_Click(object sender, EventArgs e)
    {
        if (!IsValid)
            return;
        string username, password;
        bool valid;

        username = tb_username.Text.Trim();
        password = tb_password.Text.Trim();

        valid = bal.CheckLogin(username, password);
        if (valid)
        {
            if (chk_rem.Checked)
            {
                HttpCookie u = new HttpCookie("eppUsername", username);
                u.Expires = DateTime.Now.AddMonths(1);
                HttpCookie p = new HttpCookie("eppPassword", password);
                p.Expires = DateTime.Now.AddMonths(1);
                HttpCookie login = new HttpCookie("lastlogin", DateTime.Now.ToString());
                login.Expires = DateTime.Now.AddMonths(1);

                Response.Cookies.Add(u);
                Response.Cookies.Add(p);
                Response.Cookies.Add(login);
            }
            else
            {
                Response.Cookies.Remove("eppUsername");
                Response.Cookies.Remove("eppPassword");
                Response.Cookies.Remove("lastlogin");
            }
            Server.Transfer("Default.aspx");
        }
        else
        {
            lbl_error.Text = "The username and password you entered were incorrect.";
            lbl_error.Visible = true;
        }
    }
}
