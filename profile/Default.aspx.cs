﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class profile_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["usertype"].ToString().Equals("customer", StringComparison.OrdinalIgnoreCase))
            Response.Redirect("customer.aspx");
        else if (Session["usertype"].ToString().Equals("sp", StringComparison.OrdinalIgnoreCase))
            Response.Redirect("sp.aspx");
        else
            Response.Redirect("rest.aspx");
        
    }
}
