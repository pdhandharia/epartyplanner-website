﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="customer.aspx.cs" Inherits="profile_customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cph_nav" Runat="Server">
<div  id="nav" class="home">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Customer Information:
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="cph_mainContent" Runat="Server">

<p class="float-right profilepic">
<asp:Image ID="img_profilepic" runat="server" ImageAlign="Middle" Width="128" 
    Height="128" ImageUrl="~/images/uploads/default2.png" BorderWidth="1px" /><br /><br />
<asp:FileUpload ID="FileUpload1" runat="server" /><br />
<asp:LinkButton ID="LinkButton1" runat="server">Upload new</asp:LinkButton><br clear="all" />
</p>

<div>
<h2>Profile Information:</h2>
<h3>Name: </h3>
<p>
    <asp:Label ID="lbl_name" runat="server"></asp:Label>
    </p>
<h3>Address: </h3>
<p>
    <asp:Label ID="lbl_addr" runat="server"></asp:Label>
    </p>
<h3>Mobile No.: </h3>
<p>
    <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
    </p>
<h3>Email Address: </h3>
<p>
    <asp:Label ID="lbl_email" runat="server"></asp:Label>
    </p>
<h3>Member since: </h3>
<p>
    <asp:Label ID="lbl_date" runat="server"></asp:Label>
    </p>
   <hr />
</div>

<div>
<h2>My Orders</h2>
<p>

    <asp:FormView ID="fv_ordes" runat="server" DataKeyNames="order_id" 
        DataSourceID="ds_custOrders" HeaderText=" ">
        <PagerSettings FirstPageImageUrl="~/images/resultset_first.png" 
            LastPageImageUrl="~/images/resultset_last.png" Mode="NextPreviousFirstLast" 
            NextPageImageUrl="~/images/resultset_next.png" PageButtonCount="5" 
            PreviousPageImageUrl="~/images/resultset_previous.png" />
        <EditItemTemplate>
            order_id:
            <asp:Label ID="order_idLabel1" runat="server" Text='<%# Eval("order_id") %>' />
            <br />
            placed_on:
            <asp:TextBox ID="placed_onTextBox" runat="server" 
                Text='<%# Bind("placed_on") %>' />
            <br />
            delivery_on:
            <asp:TextBox ID="delivery_onTextBox" runat="server" 
                Text='<%# Bind("delivery_on") %>' />
            <br />
            status:
            <asp:TextBox ID="statusTextBox" runat="server" Text='<%# Bind("status") %>' />
            <br />
            amount:
            <asp:TextBox ID="amountTextBox" runat="server" Text='<%# Bind("amount") %>' />
            <br />
            hasAuction:
            <asp:CheckBox ID="hasAuctionCheckBox" runat="server" 
                Checked='<%# Bind("hasAuction") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            placed_on:
            <asp:TextBox ID="placed_onTextBox" runat="server" 
                Text='<%# Bind("placed_on") %>' />
            <br />
            delivery_on:
            <asp:TextBox ID="delivery_onTextBox" runat="server" 
                Text='<%# Bind("delivery_on") %>' />
            <br />
            status:
            <asp:TextBox ID="statusTextBox" runat="server" Text='<%# Bind("status") %>' />
            <br />
            amount:
            <asp:TextBox ID="amountTextBox" runat="server" Text='<%# Bind("amount") %>' />
            <br />
            hasAuction:
            <asp:CheckBox ID="hasAuctionCheckBox" runat="server" 
                Checked='<%# Bind("hasAuction") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            Order ID:
            <asp:Label ID="order_idLabel" runat="server" Text='<%# Eval("order_id") %>' />
            <br />
            Order Placed On:
            <asp:Label ID="placed_onLabel" runat="server" Text='<%# Bind("placed_on") %>' />
            <br />
            Delivery Date:
            <asp:Label ID="delivery_onLabel" runat="server" 
                Text='<%# Bind("delivery_on") %>' />
            <br />
            Status:
            <asp:Label ID="statusLabel" runat="server" Text='<%# Bind("status") %>' />
            <br />
            Amoutnt:
            <asp:Label ID="amountLabel" runat="server" Text='<%# Bind("amount") %>' />
            <br />
            Includes auction?:
            <asp:Label ID="auctionLabel" runat="server" Text='<%# Bind("hasAuction") %>'></asp:Label>
            <br />
        </ItemTemplate>
        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        <HeaderStyle BackColor="Black" HorizontalAlign="Center" 
            VerticalAlign="Middle" ForeColor="White" />
    </asp:FormView>
    <asp:SqlDataSource ID="ds_custOrders" runat="server" 
        ConnectionString="<%$ ConnectionStrings:eppConnStr %>" 
        SelectCommand="SELECT orders.order_id, orders.placed_on, orders.delivery_on, orders.status, orders.amount, orders.hasAuction FROM customers CROSS JOIN orders">
    </asp:SqlDataSource>

</p>
<hr />
</div>

<div>
<h2>My Auctions</h2>
<p></p>
</div>

<div>
<h2>My Bookmarks</h2>
<p></p>
</div>
</asp:Content>

