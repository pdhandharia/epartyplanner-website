﻿<%@ Page Title="Search Service Providers" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="find_sp.aspx.cs" Inherits="search_find_sp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cph_nav" Runat="Server">
<div  id="nav" class="search">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Search Service Providers
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="cph_mainContent" Runat="Server">
<asp:SqlDataSource ID="ds_spcat" runat="server" 
        ConnectionString="<%$ ConnectionStrings:eppConnStr %>" EnableCaching="True" 
        SelectCommand="getCategories" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<p>
        Name:
        <asp:TextBox ID="tb_spname" runat="server"></asp:TextBox>
</p>
    <p>
        Category:
        <asp:ListBox ID="lst_spcat" runat="server" AppendDataBoundItems="True" 
            DataSourceID="ds_spcat" DataTextField="name" DataValueField="name" Rows="1">
            <asp:ListItem Value="0">- Select Category -</asp:ListItem>
        </asp:ListBox><br />
</p>
<p style="border-bottom: 1px solid #ccc;">
        <asp:Button ID="cmd_sp" runat="server" Text="Search" /><br />
</p>
<br />
<h2>Search Results:</h2>
<p>

</p>
</asp:Content>

