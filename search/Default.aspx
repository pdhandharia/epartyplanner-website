﻿<%@ Page Title="Search" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="search_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
<style type="text/css">
    ul.links
    {
        list-style-type: square;
    }
    ul.links li
    {
        padding: 2px;
        margin: 2px;
    }
</style>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cph_nav" Runat="Server">
<div  id="nav" class="search">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
</div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Search
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="cph_mainContent" Runat="Server">
<p>
Search for:</p>
<ul class="links">
    <li><a href="find_rest.aspx">Restaurants</a></li>
    <li><a href="find_sp.aspx">Service Providers</a></li>
</ul>
</asp:Content>

