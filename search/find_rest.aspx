﻿<%@ Page Title="Search Restaurants" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="find_rest.aspx.cs" Inherits="search_find_rest" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
<style type="text/css">
    
    .noborder
    {
        border: none;
    }
    tr.noborder td table
    {
        border: none;
    }

</style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cph_nav" Runat="Server">
<div  id="nav" class="search">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Search Restaurants
</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="cph_mainContent" Runat="Server">
    
    
    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        
        
    <asp:SqlDataSource ID="ds_areas" runat="server" 
        ConnectionString="<%$ ConnectionStrings:eppConnStr %>" EnableCaching="True" 
        SelectCommand="getRestAreas" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="ds_cuisines" runat="server" 
        ConnectionString="<%$ ConnectionStrings:eppConnStr %>" EnableCaching="True" 
        SelectCommand="getCuisines" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="ds_restaurants" runat="server" 
            ConnectionString="<%$ ConnectionStrings:eppConnStr %>" 
            SelectCommand="getRestaurants" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>


    <p>
        Name:         <asp:TextBox ID="tb_rname" runat="server"></asp:TextBox>
&nbsp;</p>
    <p>
        Area:
        <asp:ListBox ID="lst_area" runat="server" AppendDataBoundItems="True" 
            DataSourceID="ds_areas" DataTextField="area" DataValueField="area" Rows="1" >
            <asp:ListItem Value="0">- Select Area -</asp:ListItem>
        </asp:ListBox>
</p>
    <p>
        Cuisine:         <asp:ListBox ID="lst_cuisines" runat="server" AppendDataBoundItems="True" 
            DataSourceID="ds_cuisines" DataTextField="name" DataValueField="name" Rows="1">
            <asp:ListItem Value="0">- Select Cuisine -</asp:ListItem>
        </asp:ListBox>
</p>
    <p style="border-bottom: 1px solid #ccc;">
        <asp:Button ID="cmd_rest" runat="server" onclick="cmd_rest_Click" 
            Text="Search" />
&nbsp;&nbsp;
        <asp:Button ID="cmd_all" runat="server" onclick="cmd_all_Click" 
            Text="Show All" />
        <br />
</p>
<br />
<h2>Search Results:</h2>
    <p>
        <asp:Label ID="lbl_result" runat="server" Text="" Visible="false"></asp:Label>
        <asp:GridView ID="gv_rest" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="rest_id" 
            DataSourceID="ds_restaurants" Width="95%" GridLines="None" 
        HorizontalAlign="Left" CssClass="noborder" Visible="False" 
            ondatabound="gv_rest_DataBound">
            <PagerSettings FirstPageImageUrl="~/images/resultset_first.png" 
                LastPageImageUrl="~/images/resultset_last.png" 
                NextPageImageUrl="~/images/resultset_next.png" PageButtonCount="5" 
                PreviousPageImageUrl="~/images/resultset_previous.png" 
                Mode="NextPreviousFirstLast" />
            <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Restaurant Name" 
                    SortExpression="name" />
                <asp:BoundField DataField="area" HeaderText="Area" SortExpression="area" />
                <asp:BoundField DataField="primary_cuisine" HeaderText="Primary Cuisine" 
                    SortExpression="primary_cuisine" />
                <asp:BoundField DataField="sec_cuisine" HeaderText="Other Cuisines" 
                    NullDisplayText="NA" SortExpression="sec_cuisine" />
                <asp:BoundField DataField="rating" HeaderText="Rating" 
                    SortExpression="rating" />
                <asp:CommandField ButtonType="Image" SelectImageUrl="~/images/info.png" 
                    SelectText="View Details" ShowSelectButton="True" />
            </Columns>
            <PagerStyle 
                CssClass="noborder" HorizontalAlign="Center" VerticalAlign="Middle" />
            <SelectedRowStyle CssClass="row-a" />
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <AlternatingRowStyle CssClass="row-b" />
        </asp:GridView>
        
    </p>
    <p>
    <asp:Label ID="lbl_rdetails" runat="server" Text=""></asp:Label>
    </p>
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>