﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class search_find_rest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    
    protected void cmd_rest_Click(object sender, EventArgs e)
    {
        string str = "";
        if (!tb_rname.Text.Equals(""))
            str = "name LIKE '%" + tb_rname.Text + "%'";

        if (lst_area.SelectedValue != "0")
        {
            if (str.Length != 0)
                str += " AND ";
            str += "area LIKE '" + lst_area.SelectedValue + "'";
        }

        if (lst_cuisines.SelectedValue != "0")
        {
            if (str.Length != 0)
                str += " AND ";
            str += "primary_cuisine LIKE '" + lst_cuisines.SelectedValue + "'";
            //str += "' OR sec_suisine LIKE '" + lst_cuisines.SelectedValue + "'";
        }

        ds_restaurants.FilterExpression = str;
        //if (gv_rest.Rows.Count == 0)
        //{
        //    lbl_result.Text = "Sorry, no restaurants match you search criteria. Try some other parameters.";
        //    lbl_result.Visible = true;
        //}
        //else
        //{
        //    lbl_result.Visible = false;
        //    gv_rest.Visible = true;
        //}
    }
    protected void cmd_all_Click(object sender, EventArgs e)
    {
        lbl_result.Visible = false;
        ds_restaurants.FilterExpression = "";
        gv_rest.DataBind();
        //gv_rest.Visible = true;
    }
    protected void gv_rest_DataBound(object sender, EventArgs e)
    {
        if (gv_rest.Rows.Count == 0)
        {
            lbl_result.Text = "Sorry, no restaurants match you search criteria. Try some other parameters.";
            lbl_result.Visible = true;
        }
        else
        {
            lbl_result.Visible = false;
            gv_rest.Visible = true;
        }
    }
}