﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ajaxtest.aspx.cs" Inherits="Default2" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <img src="images/loading.gif" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="background-color:Aqua;padding: 20px">
<asp:Label ID="lblTime" runat="server" Font-Bold="True"></asp:Label>
<br />
<br />
<asp:Button ID="cmdRefreshTime" runat="server"
OnClick="cmdRefreshTime_Click"
Text="Refresh Time" />
</div>
</ContentTemplate>
</asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
