﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default2" Title="Welcome to ePartyPlanner!!" %>

<asp:Content ID="header" ContentPlaceHolderID="cph_header" runat="server">
</asp:Content>


<asp:Content ID="navbar" ContentPlaceHolderID="cph_nav" runat="server">
    <div  id="nav" class="home">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="register/Default.aspx" id="add">Register</a></li>	
            <li><a href="contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
    </div>
</asp:Content>

<asp:Content ID="pageTitle" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
    Welcome to ePartyPlanner!!
</asp:Content>

<asp:Content ID="mainContent" ContentPlaceHolderID="cph_mainContent" Runat="Server">
    <p>
    Welcome to ePartyPlanner!!
    </p>
    <h1>Here's why ePartyPlanner works: </h1>
    <h2>for an ordinary customer...</h2>
    <ul>
            <li>Direct access to hundreds of professionals providing services like photography, DJing, costumes, decorations, musicians etc.</li>
            <li>Easy to use</li>
            <li>Easy payment using PayPal</li>
            <li>Order the best food from the best restaurants in town</li>
            <li>And some more reasons too!!</li>
    </ul>    
    <h2>and for a service provider...</h2>
    <ul>
        <li>Get access to thousands of opportunities to prove your worth.</li>
        <li>Get immediate feedback from your customers.</li>
        <li>And some more advantageous...</li>
    </ul>
        
    <p>
        &nbsp;</p>
    
</asp:Content>
