﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="register_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cph_nav" Runat="Server">
    <div  id="nav" class="add">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
Register as a new...
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="cph_mainContent" Runat="Server">
<ul style="list-style:none; padding: 5px; margin: 5px;">
    <li><a href="reg_customer.aspx">Customer</a></li>
    <li><a href="reg_restaurant.aspx">Restaurant proprietor</a></li>
    <li><a href="reg_sp.aspx">Service Provider</a></li>
</ul>
</asp:Content>