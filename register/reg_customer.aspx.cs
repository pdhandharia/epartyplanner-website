﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class register : System.Web.UI.Page
{
    BusinessAccessLayer bal = new BusinessAccessLayer();


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get verification code from BAL and create a graphic to check that the user is a human and not a bot.
        
    }

 
    protected void cmd_submit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == false)
            return;
        if (bal.CheckUsername(tb_username.Text.Trim()) == false)
        {
            lbl_username.Visible = true;
            lbl_username.Text = "The username you selected is not available. Please select another username.";
            return;
        }

        string fname, lname, addr1, addr2, pin, city, mobile, email;
        string username, password, question, answer;
        char minit;
        int result;
        
        username = tb_username.Text.Trim();
        password = tb_pswd.Text.Trim();
        question = tb_ques.Text.Trim();
        answer = tb_ans.Text.Trim();
        
        fname = tb_fname.Text.Trim();
        minit = (tb_minit.Text.ToCharArray())[0];
        lname = tb_lname.Text.Trim();
        addr1 = tb_addr1.Text.Trim();
        addr2 = tb_addr2.Text.Trim();
        pin = tb_pincode.Text.Trim();
        city = tb_city.Text.Trim();
        mobile = tb_mobile.Text.Trim();
        email = tb_email.Text.Trim();
        
        result=bal.NewCustomer(fname, lname, minit, addr1, addr2, pin, city, mobile, email, username, password, question, answer);

        if (result > 0)
            Server.Transfer("../profile/Default.aspx");
        else
            Server.Transfer("../error/Default.aspx?p=cust");
    }
    
    
    
}
