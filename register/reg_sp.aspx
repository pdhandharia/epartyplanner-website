﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="reg_sp.aspx.cs" Inherits="reg_sp" Title="ePartyPlanner : : Service Provider Registration" %>

<%@ Register assembly="System.Web.DynamicData, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.DynamicData" tagprefix="cc1" %>
<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>


<asp:Content ID="navbar" ContentPlaceHolderID="cph_nav" runat="server">
        <div  id="nav" class="add">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
        </div>

</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
    Register :: Service Provider 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_mainContent" Runat="Server">
    <h2>Login Details:
    </h2>
    <p>Username: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_username" runat="server" MaxLength="20"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_username" runat="server" 
            ControlToValidate="tb_username" Display="Dynamic" 
            ErrorMessage="Please enter a username."></asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="lbl_username" runat="server" Font-Bold="True" 
            Font-Size="X-Small" ForeColor="#FF3300" Visible="False"></asp:Label>
    </p>
    <p>Password: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_pswd" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
    &nbsp;
    <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
            ControlToValidate="tb_pswd" Display="Dynamic" 
            ErrorMessage="Please enter a password."></asp:RequiredFieldValidator>
    </p>
    <p>Re-enter password: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_pswd2" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_pswd2" runat="server" 
            ControlToValidate="tb_pswd2" Display="Dynamic" 
            ErrorMessage="Please re-enter the password."></asp:RequiredFieldValidator>
&nbsp;<asp:CompareValidator ID="cmp_pswds" runat="server" ControlToCompare="tb_pswd" 
            ControlToValidate="tb_pswd2" Display="Dynamic" 
            ErrorMessage="The passwords you entered do not match."></asp:CompareValidator>
    </p>
    <p>Security question: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_ques" runat="server" MaxLength="50"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_ques" runat="server" 
            ControlToValidate="tb_ques" ErrorMessage="Please enter a security question."></asp:RequiredFieldValidator>
    </p>
    <p>Security answer: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_ans" runat="server" MaxLength="50"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_ans" runat="server" 
            ControlToValidate="tb_ans" 
            ErrorMessage="Please enter a security question answer."></asp:RequiredFieldValidator>
    </p>
    <h2>
        <br />
        Personal Details:</h2>
    <p>
        First Name: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_fname" runat="server" MaxLength="15"></asp:TextBox>
        
    &nbsp;<asp:RequiredFieldValidator ID="rfv_fname" runat="server" 
            ControlToValidate="tb_fname" Display="Dynamic" 
            ErrorMessage="Please enter your first name."></asp:RequiredFieldValidator>
        
    </p>
    <p>
        Middle Initial:<br />
        <asp:TextBox ID="tb_minit" runat="server" MaxLength="1" Width="50px"></asp:TextBox>
    </p>
    <p>
        Last Name: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_lname" runat="server" MaxLength="15"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_lname" runat="server" 
            ControlToValidate="tb_lname" Display="Dynamic" 
            ErrorMessage="Please enter your last name."></asp:RequiredFieldValidator>
    </p>
    <p>
        Address (Line 1): <span class="reqd">*</span>
        <br />
        <asp:TextBox ID="tb_addr1" runat="server" MaxLength="50"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_addr1" runat="server" 
            ControlToValidate="tb_addr1" Display="Dynamic" 
            ErrorMessage="Please enter your address,"></asp:RequiredFieldValidator>
    </p>
    <p>
        Address (Line 2):<br />
        <asp:TextBox ID="tb_addr2" runat="server" MaxLength="50"></asp:TextBox>
    </p>
    <p>
        Pincode:
        <br />
        <asp:TextBox ID="tb_pincode" runat="server" MaxLength="6"></asp:TextBox>
    </p>
    <p>
        City: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_city" runat="server" MaxLength="20"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_city" runat="server" 
            ControlToValidate="tb_city" Display="Dynamic" 
            ErrorMessage="Please enter your city."></asp:RequiredFieldValidator>
    </p>
    <p>
        Mobile No.:<br />
        +91 -
        <asp:TextBox ID="tb_mobile" runat="server" MaxLength="10"></asp:TextBox>
    </p>
    <p>
        Email Address: <span class="reqd">*</span>
        <br />
        <asp:TextBox ID="tb_email" runat="server" MaxLength="40"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_email" runat="server" 
            ControlToValidate="tb_email" Display="Dynamic" 
            ErrorMessage="Please enter your email address."></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="rev_email" runat="server" 
            ControlToValidate="tb_email" Display="Dynamic" 
            ErrorMessage="Please enter a valid email address."></asp:RegularExpressionValidator>
    </p>
    <p>
        Category: <span class="reqd">*</span><br />
        <asp:ListBox ID="lst_categories" runat="server" AppendDataBoundItems="True" 
            CausesValidation="True" DataSourceID="ds_categories" DataTextField="name" 
            DataValueField="name" Rows="1" Width="150px">
            <asp:ListItem Selected="True" Value="0">-- Select a category --</asp:ListItem>
        </asp:ListBox>
        &nbsp;<asp:CustomValidator ID="val_cat" runat="server" 
            ControlToValidate="lst_categories" Display="Dynamic" 
            ErrorMessage="Please select a valid Service Provider category." 
            onservervalidate="val_cat_ServerValidate" 
            ClientValidationFunction="val_cat_ClientValidate"></asp:CustomValidator>
        <asp:SqlDataSource ID="ds_categories" runat="server" 
            ConnectionString="<%$ ConnectionStrings:eppConnStr %>" 
            SelectCommand="getCategories" 
            EnableCaching="True" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
    </p>
    <p>
        Experience: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_exp" runat="server" MaxLength="2" Width="50px"></asp:TextBox>
&nbsp;yrs.
        <asp:RequiredFieldValidator ID="rfv_exp" runat="server" 
            ControlToValidate="tb_exp" Display="Dynamic" 
            ErrorMessage="Please enter your experience in your field."></asp:RequiredFieldValidator>
    </p>
    <p>
        <br /><br />
        <asp:Button ID="cmd_submit" runat="server" Text="Submit" 
            onclick="cmd_submit_Click" />
&nbsp;
        <asp:Button ID="cmd_reset" runat="server" Text="Reset" />
    </p>
    
    <script language="C#" type="text/C#">
        void val_cat_ClientValidate(Object s, ServerValidateEventArgs e)
        {
            String s = e.Value.ToString();
            if(s=="0")
                e.IsValid=false;
            else
                e.IsValid=true;
        }
    </script>
  
</asp:Content>