﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class register_reg_restaurant : System.Web.UI.Page
{
    BusinessAccessLayer bal = new BusinessAccessLayer();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack)
        {
            if (lst_primCuisine.SelectedIndex == 0)
            {
                lst_secCuisine.Items.Clear();
                return;
            }
            lst_secCuisine.Items.Clear();
            lst_secCuisine.DataSource = src_cuisines;
            lst_secCuisine.DataTextField = "name";
            lst_secCuisine.DataValueField = "name";
            lst_secCuisine.DataBind();
            lst_secCuisine.Items.Remove(lst_primCuisine.SelectedItem);
        }
        else
        {
            lst_primCuisine.SelectedIndex = 0;
        }     
        
    }
    protected void cmd_submit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == false)
            return;
        if (bal.CheckUsername(tb_username.Text.Trim()) == false)
        {
            lbl_username.Visible = true;
            lbl_username.Text = "The username you selected is not available. Please select another username.";
            return;
        }

        string username, password, question, answer;
        string rname, pname, primCuisine, secCuisine, timings;
        string addr1, addr2, pin, city, area, landmark, contact, email, website;
        char grade;
        bool hasAC, takesCC, alcohol, x;
        int result;

        username = tb_username.Text.Trim();
        password = tb_pswd.Text.Trim();
        question = tb_ques.Text.Trim();
        answer = tb_ans.Text.Trim();

        rname = tb_rest.Text.Trim();
        pname = tb_proprietor.Text.Trim();
        primCuisine = lst_primCuisine.SelectedValue;
        secCuisine = lst_secCuisine.SelectedValue;
        timings = tb_timings.Text.Trim();
        grade = (tb_grade.Text.ToCharArray())[0];
        hasAC = Boolean.Parse(rad_hasAC.SelectedValue);
        takesCC = Boolean.Parse(rad_takesCC.SelectedValue);
        alcohol = Boolean.Parse(rad_alcohol.SelectedValue);
        
        addr1 = tb_addr1.Text.Trim();
        addr2 = tb_addr2.Text.Trim();
        pin = tb_pincode.Text.Trim();
        city = tb_city.Text.Trim();
        landmark = tb_landmark.Text.Trim();
        area = lst_area.SelectedValue;
        contact = tb_contactNo.Text.Trim();
        email = tb_email.Text.Trim();
        website = tb_website.Text.Trim();

        result = bal.NewRest(rname, addr1, addr2, city, pin, email, contact, landmark, area, pname, timings, website, hasAC, takesCC, alcohol, grade, primCuisine, secCuisine, username, password, question, answer);

        if (result > 0)
            Server.Transfer("../profile/Default.aspx");
        else
            Server.Transfer("../error/Default.aspx?p=rest");
    }
}
