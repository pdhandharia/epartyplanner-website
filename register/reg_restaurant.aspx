﻿<%@ Page Title="Register :: Restaurants" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="reg_restaurant.aspx.cs" Inherits="register_reg_restaurant" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" Runat="Server">
</asp:Content>


<asp:Content ID="navbar" ContentPlaceHolderID="cph_nav" runat="server">
        <div  id="nav" class="add">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
        </div>

</asp:Content>


<asp:Content ID="pageTitle" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
    Register :: Restaurant
</asp:Content>

<asp:Content ID="mainContent" ContentPlaceHolderID="cph_mainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <h2>Login Details</h2>
    <p>
    Username:
    <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_username" runat="server"></asp:TextBox>
    
        &nbsp;<asp:RequiredFieldValidator ID="rfv_username" runat="server" 
            ControlToValidate="tb_username" Display="Dynamic">Please enter a username.</asp:RequiredFieldValidator><br />
            <asp:Label ID="lbl_username" Visible="false" Font-Bold="True" ForeColor="#FF3300" Font-Size="X-Small" runat="server"></asp:Label>
    
    </p>
<p>
    
        Password:
    <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_pswd" runat="server" TextMode="Password"></asp:TextBox>
    
    &nbsp;<asp:RequiredFieldValidator ID="rfv_pswd" runat="server" 
            ControlToValidate="tb_pswd" Display="Dynamic">Please enter a password.</asp:RequiredFieldValidator>
            
    <cc1:PasswordStrength ID="pswd_passwordstrength" runat="server" 
            CalculationWeightings="50;20;5;25" Enabled="True" HelpStatusLabelID="lbl_pswd" 
            MinimumNumericCharacters="1" MinimumSymbolCharacters="1" 
            PreferredPasswordLength="10" TargetControlID="tb_pswd" 
            TextStrengthDescriptions="Very Poor; Average; Good; Excellent" 
            DisplayPosition="BelowRight" HelpHandlePosition="BelowRight">
    </cc1:PasswordStrength>
    
    <asp:Label ID="lbl_pswd" runat="server" Text=""></asp:Label>
    
    </p>
    <p>
    
        Re-enter password:     <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_pswd2" runat="server" TextMode="Password"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_pswd2" runat="server" 
            ControlToValidate="tb_pswd2" Display="Dynamic">Please enter the password again.</asp:RequiredFieldValidator>
    
    &nbsp;<asp:CompareValidator ID="cmp_pswd" runat="server" ControlToCompare="tb_pswd" 
            ControlToValidate="tb_pswd2" Display="Dynamic">Both the password fields must match.</asp:CompareValidator>
    
    </p>
    <p>
    
        Security question:     <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_ques" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_ques" runat="server" 
            ControlToValidate="tb_ques" Display="Dynamic">Please enter a security question.</asp:RequiredFieldValidator>
&nbsp;</p>
    <p>
    
        Security answer:     <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_ans" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_ans" runat="server" 
            ControlToValidate="tb_ans" Display="Dynamic">Please provide the answer to the above security question.</asp:RequiredFieldValidator>
        <br />
        <br />
    
    </p>
    <h2>
    
        Restaurant Details:
    </h2>
    
    <p>
        Restaurant Name:         <span class="reqd">*</span>
    <br />
    <asp:TextBox ID="tb_rest" runat="server" MaxLength="20"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_rest" runat="server" 
            ControlToValidate="tb_rest">Restaurant name is required.</asp:RequiredFieldValidator>
    <br />
    <br />
        Proprietor's Name:
    <span class="reqd">*</span><br />
    <asp:TextBox ID="tb_proprietor" runat="server" MaxLength="30"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_proprietor" runat="server" 
            ControlToValidate="tb_proprietor">Proprietor name is required.</asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:SqlDataSource ID="src_cuisines" runat="server" 
            ConnectionString="<%$ ConnectionStrings:eppConnStr %>" 
            SelectCommand="getCuisines" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        Primary Cuisine:
    <span class="reqd">*</span><br />
        <asp:ListBox ID="lst_primCuisine" runat="server" Rows="1" Width="125px" 
            AutoPostBack="True" DataSourceID="src_cuisines" DataTextField="name" 
            DataValueField="name" AppendDataBoundItems="True" CausesValidation="True">
            <asp:ListItem Value="0">- Select cuisine -</asp:ListItem>
        </asp:ListBox>
        
        &nbsp;<asp:RequiredFieldValidator ID="rfv_primCuisine" runat="server" 
            ControlToValidate="lst_primCuisine" Display="Dynamic" InitialValue="0" 
            SetFocusOnError="True">Please select the primary cuisine.</asp:RequiredFieldValidator>
        
        <br />
        <br />
        Secondary Cuisine:
    <br />
        <asp:ListBox ID="lst_secCuisine" runat="server" Rows="1" Width="125px" 
            AppendDataBoundItems="True">
        </asp:ListBox>
        <br />
        <br />
        Timings:<br />
        <asp:TextBox ID="tb_timings" runat="server"></asp:TextBox>
        <br />
        <br />
        Air-conditioned:<br />
        <asp:RadioButtonList ID="rad_hasAC" runat="server" CausesValidation="True" 
            CellPadding="0" CellSpacing="0" RepeatColumns="2" RepeatDirection="Horizontal" 
            RepeatLayout="Flow" BorderStyle="None">
            <asp:ListItem Value="Yes">Yes</asp:ListItem>
            <asp:ListItem Value="No">No</asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        Credit cards accepted:<br />
        <asp:RadioButtonList ID="rad_takesCC" runat="server" CausesValidation="True" 
            CellPadding="0" CellSpacing="0" RepeatColumns="2" RepeatDirection="Horizontal" 
            RepeatLayout="Flow">
            <asp:ListItem Value="Yes">Yes</asp:ListItem>
            <asp:ListItem Value="No">No</asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        Serves alcohol:<br />
        <asp:RadioButtonList ID="rad_alcohol" runat="server" CausesValidation="True" 
            CellPadding="0" CellSpacing="0" RepeatColumns="2" RepeatDirection="Horizontal" 
            RepeatLayout="Flow">
            <asp:ListItem Value="Yes">Yes</asp:ListItem>
            <asp:ListItem Value="No">No</asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        Grade (I / II / III):<br />
        <asp:TextBox ID="tb_grade" runat="server" MaxLength="1" Width="50px"></asp:TextBox>
        <br />
        <br />
    </p>
    <h2>
        Other Details:</h2>
    <p>
    Address Line 1: <span class="reqd">*</span><br />
    <asp:TextBox ID="tb_addr1" runat="server" MaxLength="50"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_addr1" runat="server" 
            ControlToValidate="tb_addr1">Address Line 1 Required.</asp:RequiredFieldValidator>
    <br />
    <br />
    Address Line 2:<br />
    <asp:TextBox ID="tb_addr2" runat="server" MaxLength="50"></asp:TextBox>
    <br />
    <br />
    Pincode:<br />
    <asp:TextBox ID="tb_pincode" runat="server" MaxLength="6"></asp:TextBox>
    <br />
    <br />
        City:<br />
    <asp:TextBox ID="tb_city" runat="server"></asp:TextBox>
        <br />
        <br />
        Nearest landmark (if any):<br />
        <asp:TextBox ID="tb_landmark" runat="server"></asp:TextBox>
        <br />
        <br />
        Area: <span class="reqd">*</span><br />
        <asp:ListBox ID="lst_area" runat="server" AppendDataBoundItems="True" 
            DataSourceID="src_area" DataTextField="area" DataValueField="area" Rows="1" 
            Width="125px">
            <asp:ListItem Value="0">- Select area -</asp:ListItem>
        </asp:ListBox>
        <asp:SqlDataSource ID="src_area" runat="server" 
            ConnectionString="<%$ ConnectionStrings:eppConnStr %>" 
            SelectCommand="getRestAreas" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
&nbsp;<asp:RequiredFieldValidator ID="rfv_area" runat="server" 
            ControlToValidate="lst_area" Display="Dynamic" InitialValue="0">Please enter the area in which the restaurant is located.</asp:RequiredFieldValidator>
    <br />
    <br />
    Email Address: <br />
    <asp:TextBox ID="tb_email" runat="server"></asp:TextBox>
    &nbsp;<asp:RegularExpressionValidator ID="rev_email" runat="server" 
            ControlToValidate="tb_email" Display="Dynamic" 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Please enter a valid email address.</asp:RegularExpressionValidator>
        <br />
        <br />
        Contact No.: <span class="reqd">*</span><br />
        <asp:TextBox ID="tb_contactNo" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="rfv_contactNo" runat="server" 
            ControlToValidate="tb_contactNo">Please provide a contact number.</asp:RequiredFieldValidator>
        <br />
        <br />
        Website:<br />
        <asp:TextBox ID="tb_website" runat="server"></asp:TextBox>
        <br />
        <br />
    </p>
    <p>
    <asp:Button ID="cmd_submit" runat="server" Text="Submit" 
            onclick="cmd_submit_Click" />
&nbsp;<input id="ip_reset" type="reset" value="Reset" /></p>
</asp:Content>
