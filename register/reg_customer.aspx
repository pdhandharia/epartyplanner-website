<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeFile="reg_customer.aspx.cs" Inherits="register" Title="Register : : Customer" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>


<asp:Content ID="Content1" runat="server" contentplaceholderid="cph_header">

    </asp:Content>



<asp:Content ID="navbar" ContentPlaceHolderID="cph_nav" runat="server">
        <div  id="nav" class="add">
        <ul>        
            <li><a href="../Default.aspx" id="home">Home</a></li>
            <li><a href="../search/Default.aspx" id="search">Search Listings</a></li>
            <li><a href="../order/Default.aspx" id="order">Place An Order</a></li>
            <li><a href="../register/Default.aspx" id="add">Register</a></li>	
            <li><a href="../contactus.aspx" id="contact">Contact Us</a></li>
        </ul>
        </div>

</asp:Content>


<asp:Content ID="pageTitle" ContentPlaceHolderID="cph_pageTitle" Runat="Server">
    Register :: Customer
</asp:Content>

<asp:Content ID="mainContent" ContentPlaceHolderID="cph_mainContent" Runat="Server">
    
    <h2>
                    Login Details</h2>
                <p>
                    Username: <span class="reqd">*</span><br />
                    <asp:TextBox ID="tb_username" runat="server"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                        ControlToValidate="tb_username" Display="Dynamic" 
                        ErrorMessage="Please enter a username."></asp:RequiredFieldValidator>
                    <br />
                    <asp:Label ID="lbl_username" runat="server" Visible="false" Font-Bold="True" 
                        Font-Size="Smaller" ForeColor="#FF3300"></asp:Label>
                </p>
                <p>
                    Password: <span class="reqd">*</span><br />
                    <asp:TextBox ID="tb_pswd" runat="server" TextMode="Password"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="rfv_pswd" runat="server" 
                        ControlToValidate="tb_pswd" Display="Dynamic" 
                        ErrorMessage="Please enter a password."></asp:RequiredFieldValidator>
                </p>
                <p>
                    Re-enter password: <span class="reqd">*</span><br />
                    <asp:TextBox ID="tb_pswd2" runat="server" TextMode="Password"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="rfv_pswd2" runat="server" 
                        ControlToValidate="tb_pswd2" Display="Dynamic" 
                        ErrorMessage="Please enter the password again."></asp:RequiredFieldValidator>
                </p>
                <p>
                    Security question: <span class="reqd">*</span><br />
                    <asp:TextBox ID="tb_ques" runat="server"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="rfv_ques" runat="server" 
                        ControlToValidate="tb_ques" Display="Dynamic" 
                        ErrorMessage="Please enter a security question."></asp:RequiredFieldValidator>
                    &nbsp;<asp:CompareValidator ID="cmp_pswd" runat="server" ControlToCompare="tb_pswd" 
                        ControlToValidate="tb_pswd2" Display="Dynamic" 
                        ErrorMessage="Both the password fields must match."></asp:CompareValidator>
                </p>
                <p>
                    Security answer: <span class="reqd">*</span><br />
                    <asp:TextBox ID="tb_ans" runat="server"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="rfv_ans" runat="server" 
                        ControlToValidate="tb_ans" Display="Dynamic" 
                        ErrorMessage="Please provide the answer to the above security question."></asp:RequiredFieldValidator>
                    <br />
                    <br />
                </p>
                
                
                <h2>
    
        Personal Details:
    </h2>
    
    <p>
    First Name:
    <span class="reqd">*</span><br />
    <asp:TextBox ID="tb_fname" runat="server" MaxLength="20" 
            AutoCompleteType="FirstName"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_fname" runat="server" 
            ControlToValidate="tb_fname" ErrorMessage="First Name Required."></asp:RequiredFieldValidator>
    <br />
    <br />
    Middle Initial:<br />
    <asp:TextBox ID="tb_minit" runat="server" MaxLength="1" Width="50px"></asp:TextBox>
    <br />
    <br />
    Last Name:
        <span class="reqd">*</span>
    <br />
    <asp:TextBox ID="tb_lname" runat="server" MaxLength="20" 
            AutoCompleteType="LastName"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_lname" runat="server" 
            ControlToValidate="tb_lname" ErrorMessage="Last Name Required."></asp:RequiredFieldValidator>
    <br />
    <br />
    Address Line 1: <span class="reqd">*</span><br />
    <asp:TextBox ID="tb_addr1" runat="server" MaxLength="50"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_addr1" runat="server" 
            ControlToValidate="tb_addr1" ErrorMessage="Address Line 1 Required."></asp:RequiredFieldValidator>
    <br />
    <br />
    Address Line 2:<br />
    <asp:TextBox ID="tb_addr2" runat="server" MaxLength="50"></asp:TextBox>
    <br />
    <br />
    Pincode:<br />
    <asp:TextBox ID="tb_pincode" runat="server" MaxLength="6"></asp:TextBox>
    <br />
    <br />
        City:<br />
    <asp:TextBox ID="tb_city" runat="server" AutoCompleteType="HomeCity"></asp:TextBox>
    <br />
    <br />
    Email Address: <span class="reqd">*</span><br />
    <asp:TextBox ID="tb_email" runat="server" AutoCompleteType="Email"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_email" runat="server" 
            ControlToValidate="tb_email" ErrorMessage="Email Address Required."></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="rev_email" runat="server" 
            ControlToValidate="tb_email" Display="Dynamic" 
            ErrorMessage="Please enter a valid email address." 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <br />
    <br />
    Mobile No.: <span class="reqd">*</span><br />
    +91 - <asp:TextBox ID="tb_mobile" runat="server" MaxLength="10" 
            AutoCompleteType="Cellular"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="rfv_mobile" runat="server" 
            ControlToValidate="tb_mobile" ErrorMessage="Mobile No. Required."></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="rev_mobile" runat="server" 
            ControlToValidate="tb_mobile" Display="Dynamic" 
            ErrorMessage="Please enter a valid mobile number." 
            ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
        <br />
    <br />
    </p>
    
    <p>
    <asp:Button ID="cmd_submit" runat="server" Text="Submit" 
            onclick="cmd_submit_Click" />
&nbsp;<input id="ip_reset" type="reset" value="Reset" /></p>
    
       
    </asp:Content>
