﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_greeting.Text = Session["greeting"].ToString();
        if (Session["greeting"].ToString().Equals("guest", StringComparison.OrdinalIgnoreCase))
        {
            lbl_options.Text = "<a href='../profile/login.aspx'>login</a> | <a href='../register/default.aspx'>register</a>";
        }
        else
        {
            lbl_options.Text = "<a href='../profile/editprofile.aspx'>edit profile</a> | <a href='../profile/logout.aspx'>logout</a>";
        }
    }
}
